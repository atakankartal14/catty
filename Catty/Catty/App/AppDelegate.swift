//
//  AppDelegate.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        let root = BreedsListBuilder.create()
        window?.rootViewController = root
        window?.makeKeyAndVisible()
        
        return true
    }
}

