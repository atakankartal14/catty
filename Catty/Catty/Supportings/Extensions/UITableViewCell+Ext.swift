//
//  UITableViewCell+Ext.swift
//  Catty
//
//  Created by Atakan Kartal on 8.04.2022.
//

import UIKit

extension UITableView {

    /// Dequeues cell with class type and indexPath
    func dequeueReusableCell<T: UITableViewCell>(withClass name: T.Type, for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: String(describing: name), for: indexPath) as? T else {
            fatalError("\(C.Strings.Errors.noCell) \(String(describing: name))")
        }
        return cell
    }

    /// Registers cell with class  type
    func register<T: UITableViewCell>(cellWithClass name: T.Type) {
        let nibName = String(describing: name)
        let nib = UINib(nibName: nibName, bundle: nil)
        register(nib, forCellReuseIdentifier: nibName)
    }
}
