//
//  Optionals+Ext.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Foundation

extension Optional where Wrapped == String {

    // Returns empty string if value is optional
    var orEmpty: String {
        self ?? ""
    }

    // Convert given string to URL
    var asURL: URL? {
        URL(string: self.orEmpty)
    }
}

extension Optional where Wrapped == Int {

    // Returns zero if value is optional
    var orZero: Int {
        self ?? .zero
    }
}
