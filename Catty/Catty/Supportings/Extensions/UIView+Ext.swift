//
//  UIView+Ext.swift
//  Catty
//
//  Created by Atakan Kartal on 8.04.2022.
//

import UIKit

protocol NibLoadable {

    /// The content of the UIView
    var contentView: UIView! { get }

    /// Attach a custom `Nib` to the view's content
    func commonInit()
}

extension NibLoadable where Self: UIView {

    func commonInit() {
        Bundle.main.loadNibNamed(String(describing: Self.self), owner: self, options: nil)
        addSubview(contentView)
        contentView.backgroundColor = .clear
        contentView.frame = bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        contentView.translatesAutoresizingMaskIntoConstraints = true
    }

}
