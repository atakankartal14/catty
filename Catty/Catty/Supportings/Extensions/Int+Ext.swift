//
//  Int+Ext.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Foundation

extension Int {

    /// Converts int to String
    var asString: String {
        String(self)
    }
}
