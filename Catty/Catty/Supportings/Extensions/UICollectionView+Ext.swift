//
//  UICollectionView+Ext.swift
//  Catty
//
//  Created by Atakan Kartal on 8.04.2022.
//

import UIKit

extension UICollectionView {

    /// Dequeues cell with class type and indexPath
    func dequeueReusableCell<T: UICollectionViewCell>(withClass name: T.Type, for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withReuseIdentifier: String(describing: name), for: indexPath) as? T else {
            fatalError("\(C.Strings.Errors.noCell) \(String(describing: name))")
        }
        return cell
    }

    /// Registers cell with class  type
    func register<T: UICollectionViewCell>(cellWithClass name: T.Type) {
        let nib = UINib(nibName: String(describing: T.self), bundle: nil)
        register(nib, forCellWithReuseIdentifier: String(describing: name))
    }
}
