//
//  BreedDetailViewController.swift
//  Catty
//
//  Created by Atakan Kartal on 8.04.2022.
//

import UIKit

final class BreedDetailViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet private weak var tableView: UITableView!

    // MARK: - Properties
    var presenter: BreedDetailViewOutput!
    private var tableViewDelegate: BreedDetailTableViewDelegateInput
    private var tableViewDataSource: BreedDetailTableViewDataSourceInput

    init(tableViewDelegate: BreedDetailTableViewDelegateInput,
         tableViewDataSource: BreedDetailTableViewDataSourceInput) {
        self.tableViewDelegate = tableViewDelegate
        self.tableViewDataSource = tableViewDataSource
        super.init(nibName: String(describing: Self.self), bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life-Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.viewDidLoad()
    }
}

// MARK: - Input
extension BreedDetailViewController: BreedDetailViewInput {
    func configureTableView() {
        tableView.register(cellWithClass: BreedDetailTableViewCell.self)
        tableView.delegate = tableViewDelegate
        tableView.dataSource = tableViewDataSource
    }
    
    func reload(with model: BreedDetailPresentationProtocol) {
        tableViewDelegate.update(with: model.imageUrl)
        tableViewDataSource.update(with: model.items)
        tableView.reloadData()
    }
}
