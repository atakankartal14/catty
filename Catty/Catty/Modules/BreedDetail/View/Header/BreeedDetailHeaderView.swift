//
//  BreeedDetailHeaderView.swift
//  Catty
//
//  Created by Atakan Kartal on 8.04.2022.
//

import UIKit
import Kingfisher

final class BreeedDetailHeaderView: UIView, NibLoadable {

    private enum Constants {
        static let cornerRadius: CGFloat = 20
    }

    @IBOutlet var contentView: UIView!
    @IBOutlet private weak var breedImageView: UIImageView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    override func layoutSubviews() {
        super.layoutSubviews()

        breedImageView.layer.masksToBounds = true
        breedImageView.clipsToBounds = true
        breedImageView.layer.cornerRadius = Constants.cornerRadius
    }

    func configure(with url: URL?) {
        let processor = DownsamplingImageProcessor(size: breedImageView.bounds.size)
        |> RoundCornerImageProcessor(cornerRadius: Constants.cornerRadius)
        breedImageView.kf.indicatorType = .activity
        breedImageView.kf.setImage(
            with: url,
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
    }
}
