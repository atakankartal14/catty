//
//  BreedDetailTableViewCell.swift
//  Catty
//
//  Created by Atakan Kartal on 8.04.2022.
//

import UIKit

final class BreedDetailTableViewCell: UITableViewCell {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var detailLabel: UILabel!

    func configure(with item: BreedDetailItemProtocol) {
        titleLabel.text = item.title
        detailLabel.text = item.detail
    }
}
