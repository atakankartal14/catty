//
//  BreedDetailTableViewDelegate.swift
//  Catty
//
//  Created by Atakan Kartal on 8.04.2022.
//

import UIKit

protocol BreedDetailTableViewDelegateInput: UITableViewDelegate {

    func update(with url: URL?)
}

final class BreedDetailTableViewDelegate: NSObject, BreedDetailTableViewDelegateInput {

    private var imageUrl: URL?
    
    func update(with imageUrl: URL?) {
        self.imageUrl = imageUrl
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = BreeedDetailHeaderView(frame: .zero)
        view.configure(with: imageUrl)
        return view
    }
}

