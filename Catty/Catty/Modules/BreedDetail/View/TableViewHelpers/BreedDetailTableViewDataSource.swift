//
//  BreedDetailTableViewDataSource.swift
//  Catty
//
//  Created by Atakan Kartal on 8.04.2022.
//

import UIKit

protocol BreedDetailTableViewDataSourceInput: UITableViewDataSource {

    func update(with items: [BreedDetailItemProtocol])
}

final class BreedDetailTableViewDataSource: NSObject, BreedDetailTableViewDataSourceInput {
    
    var items = [BreedDetailItemProtocol]()
    
    func update(with items: [BreedDetailItemProtocol]) {
        self.items = items
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withClass: BreedDetailTableViewCell.self, for: indexPath)
        let item = items[indexPath.row]
        cell.configure(with: item)
        return cell
    }

}
