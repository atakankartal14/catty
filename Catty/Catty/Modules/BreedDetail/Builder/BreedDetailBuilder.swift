//
//  BreedDetailBuilder.swift
//  Catty
//
//  Created by Atakan Kartal on 8.04.2022.
//

import UIKit

enum BreedDetailBuilder {

    static func create(from item: BreedProtocol) -> UIViewController {
        let tableViewDelegate = BreedDetailTableViewDelegate()
        let tableViewDataSource = BreedDetailTableViewDataSource()
        let view = BreedDetailViewController(tableViewDelegate: tableViewDelegate,
                                             tableViewDataSource: tableViewDataSource)
        
        let interactor = BreedDetailInteractor(breed: item)
        
        let presenter = BreedDetailPresenter(view: view, interactor: interactor)
        
        view.presenter = presenter
        interactor.presenter = presenter
        
        return view
    }
}
