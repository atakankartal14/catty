//
//  BreedDetailPresenter.swift
//  Catty
//
//  Created by Atakan Kartal on 8.04.2022.
//

import Foundation

final class BreedDetailPresenter {

    weak var view: BreedDetailViewInput!
    var interactor: BreedDetailInteractorInput!
    
    init(view: BreedDetailViewInput, interactor: BreedDetailInteractorInput) {
        self.view = view
        self.interactor = interactor
    }
}

// MARK: - View Output
extension BreedDetailPresenter: BreedDetailViewOutput {
    func viewDidLoad() {
        view.configureTableView()
        interactor.fetch()
    }
}

// MARK: - Interactor Output
extension BreedDetailPresenter: BreedDetailInteractorOutput {
    func interactor(_ interactor: BreedDetailInteractorInput,
                    didReceiveModel model: BreedDetailPresentationProtocol) {
        view.reload(with: model)
    }
}
