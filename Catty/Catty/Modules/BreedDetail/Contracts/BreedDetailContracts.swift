//
//  BreedDetailContracts.swift
//  Catty
//
//  Created by Atakan Kartal on 8.04.2022.
//

import Foundation

// MARK: - View
protocol BreedDetailViewInput: AnyObject {

    func configureTableView()

    func reload(with model: BreedDetailPresentationProtocol)
}

protocol BreedDetailViewOutput: AnyObject {

    func viewDidLoad()
}

// MARK: - Interactor
protocol BreedDetailInteractorInput: AnyObject {

    func fetch()
}

protocol BreedDetailInteractorOutput: AnyObject {

    func interactor(_ interactor: BreedDetailInteractorInput, didReceiveModel model: BreedDetailPresentationProtocol)
}
