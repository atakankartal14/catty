//
//  BreedDetailInteractor.swift
//  Catty
//
//  Created by Atakan Kartal on 8.04.2022.
//

import Foundation

final class BreedDetailInteractor {

    weak var presenter: BreedDetailInteractorOutput!

    private var breed: BreedProtocol
    
    init(breed: BreedProtocol) {
        self.breed = breed
    }
}

// MARK: - Input
extension BreedDetailInteractor: BreedDetailInteractorInput {
    func fetch() {
        typealias T = C.Strings.Text
        let items: [BreedDetailItemPresentationModel] = [
            .init(title: T.name, detail: breed.name),
            .init(title: T.temperament, detail: breed.temperament),
            .init(title: T.energyLevel, detail: breed.energyLevel),
        ]
        let model = BreedDetailPresentationModel(imageUrl: breed.imageUrl, items: items)
        presenter.interactor(self, didReceiveModel: model)
    }
}
