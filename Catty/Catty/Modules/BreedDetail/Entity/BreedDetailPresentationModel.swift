//
//  BreedDetailPresentationModel.swift
//  Catty
//
//  Created by Atakan Kartal on 8.04.2022.
//

import Foundation

protocol BreedDetailPresentationProtocol {

    var imageUrl: URL? { get set }

    var items: [BreedDetailItemProtocol] { get set }
}

struct BreedDetailPresentationModel: BreedDetailPresentationProtocol {

    var imageUrl: URL?    
    var items: [BreedDetailItemProtocol]
}
