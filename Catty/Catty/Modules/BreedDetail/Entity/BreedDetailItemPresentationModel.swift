//
//  BreedDetailPresentationModel.swift
//  Catty
//
//  Created by Atakan Kartal on 8.04.2022.
//

import Foundation

protocol BreedDetailItemProtocol {

    var title: String { get set }

    var detail: String { get set }
}

struct BreedDetailItemPresentationModel: BreedDetailItemProtocol {

    var title: String
    var detail: String
}
