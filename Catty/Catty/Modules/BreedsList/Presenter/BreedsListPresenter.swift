//
//  BreedsListPresenter.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Foundation

final class BreedsListPresenter {
    
    private enum Constants {
        static let queryGapSecond = 0.8
    }

    weak var view: BreedsListViewInput!
    var interactor: BreedsListInteractorInput!
    var router: BreedsListRouterInput!
    
    init(view: BreedsListViewInput, interactor: BreedsListInteractorInput, router: BreedsListRouterInput) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }

    private var items = [BreedProtocol]()
    private var searchDispatchWorkItem: DispatchWorkItem?

    // MARK: - Search
    private func search(_ query: String) {
        searchDispatchWorkItem?.cancel()
        setSearchDispatchWorkItem(with: query)

        guard let workItem = searchDispatchWorkItem else { return }

        DispatchQueue.global().asyncAfter(deadline: .now() + Constants.queryGapSecond,
                                          execute: workItem)
    }

    private func setSearchDispatchWorkItem(with text: String) {
        searchDispatchWorkItem = DispatchWorkItem { [weak self] in
            guard let self = self else { return }
            self.view.showLoading()
            self.interactor.search(text)
        }
    }

    // MARK: - Item handling
    private func reloadItems(_ items: [BreedProtocol]) {
        self.items = items
        view.hideLoading()
        view.reload(with: items)
    }
}

// MARK: - View Outut
extension BreedsListPresenter: BreedsListViewOutput {
    func viewDidLoad() {
        view.setTitle(C.Strings.Title.breedsList)
        view.configureCollectionView()
        view.showLoading()
        interactor.fetch()
    }

    func didReachEndOfPage() {
        if interactor.canFetchNext() {
            view.showLoading()
            interactor.fetchNextPage()
        }
    }

    func didSelectItem(at index: Int) {
        router.showDetail(with: items[index])
    }

    func queryTextDidChange(_ query: String) {
        search(query)
    }
}

// MARK: - Interactor Outut
extension BreedsListPresenter: BreedsListInteractorOutput {
    func interactor(_ interactor: BreedsListInteractorInput, didReceiveItems items: [BreedProtocol]) {
        reloadItems(items)
    }

    func interactor(_ interactor: BreedsListInteractorInput, didReceiveQueryResults items: [BreedProtocol]) {
        reloadItems(items)
    }

    func interactor(_ interactor: BreedsListInteractorInput, didFailWith error: APIError) {
        view.hideLoading()
        router.showAlert(title: error.title, message: error.message)
    }
}
