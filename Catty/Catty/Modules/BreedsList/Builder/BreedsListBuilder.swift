//
//  BreedsListBuilder.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import UIKit
import Moya

enum BreedsListBuilder {

    static func create() -> UINavigationController {
        let provider = API.breedsProvider
        let interactor = BreedsListInteractor(provider: provider)

        let dataSource = BreedsListCollectionViewDataSource()
        let delegate = BreedsListCollectionViewDelegate()
        let view = BreedsListViewController(collectionViewDelegate: delegate, collectionViewDataSource: dataSource)
        
        let navigationController = UINavigationController(rootViewController: view)
        let router = BreedsListRouter()
        router.navigationController = navigationController
        
        let presenter = BreedsListPresenter(view: view, interactor: interactor, router: router)
        
        view.presenter = presenter
        interactor.presenter = presenter
        
        return navigationController
    }
}
