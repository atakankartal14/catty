//
//  BreedsListRouter.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import UIKit

final class BreedsListRouter {

    var navigationController: UINavigationController!
}

extension BreedsListRouter: BreedsListRouterInput {
    func showDetail(with item: BreedProtocol) {
        let vc = BreedDetailBuilder.create(from: item)
        navigationController.pushViewController(vc, animated: true)
    }

    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        alert.addAction(.init(title: C.Strings.Text.ok, style: .default, handler: nil))
        navigationController.present(alert, animated: true, completion: nil)
    }
}
