//
//  BreedsListInteractor.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Foundation
import Moya

final class BreedsListInteractor {

    private enum Constants {
        static let limit = 15
        static let initialPage = Int.zero
    }
    
    weak var presenter: BreedsListInteractorOutput!

    typealias Provider = MoyaProvider<BreedsTarget<API.E>>
    
    private var provider: Provider
    private var items = [BreedProtocol]()
    private var page = Constants.initialPage
    private var isFetching = false
    private var hasNextPage = true
    private var isSearching = false

    init(provider: Provider) {
        self.provider = provider
    }

    private var breedsRequest: BreedsRequest {
        BreedsRequest(page: page, limit: Constants.limit)
    }

    private func reset() {
        items.removeAll()
        hasNextPage = false
        page = Constants.initialPage
        isSearching = false
    }
}

// MARK: - Input
extension BreedsListInteractor: BreedsListInteractorInput {
    func fetch() {
        isFetching = true

        provider.request(.breeds(breedsRequest), responseType: [BreedsResponse].self) { [weak self] result in
            guard let self = self else { return }

            self.isFetching = false

            switch result {
            case .success(let response):
                let items: [BreedPresentationModel] = response.map { .init(from: $0) }
                
                self.hasNextPage = !items.isEmpty
                
                if self.page == Constants.initialPage {
                    self.items = items
                } else {
                    self.items.append(contentsOf: items)
                }

                self.page += self.hasNextPage ? 1 : 0
                
                self.presenter.interactor(self, didReceiveItems: self.items)

            case .failure(let error):
                self.presenter.interactor(self, didFailWith: error)
            }
        }
    }
    
    func fetchNextPage() {
        fetch()
    }

    func search(_ query: String) {
        isSearching = true
        guard !query.isEmpty else {
            reset()
            fetch()
            return
        }

        provider.request(.search(.init(query: query)), responseType: [BreedsResponse].self) { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case .success(let response):
                let items: [BreedPresentationModel] = response.map { .init(from: $0) }
                self.items = items
                self.presenter.interactor(self, didReceiveQueryResults: self.items)
            case .failure(let error):
                self.presenter.interactor(self, didFailWith: error)
            }
        }
    }

    func canFetchNext() -> Bool {
        !isFetching && hasNextPage && !isSearching
    }
}
