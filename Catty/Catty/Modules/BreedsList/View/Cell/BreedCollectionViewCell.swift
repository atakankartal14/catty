//
//  BreedCollectionViewCell.swift
//  Catty
//
//  Created by Atakan Kartal on 8.04.2022.
//

import UIKit
import Kingfisher
import Alamofire

final class BreedCollectionViewCell: UICollectionViewCell {
    
    private enum Constants {
        static let cornerRadius: CGFloat = 20
    }

    @IBOutlet private weak var breedImageView: UIImageView!
    @IBOutlet private weak var breedNameLabel: UILabel!

    override func layoutSubviews() {
        super.layoutSubviews()

        breedImageView.layer.cornerRadius = Constants.cornerRadius
    }
    
    func configure(with item: BreedProtocol) {
        breedNameLabel.text = item.name
        
        let processor = DownsamplingImageProcessor(size: breedImageView.bounds.size)
        |> RoundCornerImageProcessor(cornerRadius: Constants.cornerRadius)
        breedImageView.kf.indicatorType = .activity
        breedImageView.kf.setImage(
            with: item.imageUrl,
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
    }
}
