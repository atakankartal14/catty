//
//  BreedsListViewController.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import UIKit
import ProgressHUD

final class BreedsListViewController: UIViewController {

    private enum Constants {
        static let cellNibName = String(describing: BreedCollectionViewCell.self)
    }

    // MARK: - IBOutlets
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var collectionView: UICollectionView!

    // MARK: - Properties
    var presenter: BreedsListViewOutput!
    private var collectionViewDelegate: BreedsListCollectionViewDelegateInput
    private var collectionViewDataSource: BreedsListCollectionViewDataSourceInput

    init(collectionViewDelegate: BreedsListCollectionViewDelegateInput,
         collectionViewDataSource: BreedsListCollectionViewDataSourceInput) {
        self.collectionViewDelegate = collectionViewDelegate
        self.collectionViewDataSource = collectionViewDataSource
        super.init(nibName: String(describing: Self.self), bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Life-Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        presenter.viewDidLoad()
    }
}

// MARK: - Input
extension BreedsListViewController: BreedsListViewInput {
    func setTitle(_ text: String) {
        title = text
    }

    func configureCollectionView() {
        collectionView.register(cellWithClass: BreedCollectionViewCell.self)
        collectionViewDelegate.output = self
        collectionView.delegate = collectionViewDelegate
        collectionView.dataSource = collectionViewDataSource
    }
    
    func reload(with items: [BreedProtocol]) {
        collectionViewDataSource.update(with: items)
        collectionView.reloadData()
    }
    
    func showLoading() {
        ProgressHUD.show()
    }
    
    func hideLoading() {
        ProgressHUD.dismiss()
    }
}

// MARK: - CollectionView Delegate Output
extension BreedsListViewController: BreedsListCollectionViewDelegateOutput {
    func didSelectItem(at index: Int) {
        presenter.didSelectItem(at: index)
    }

    func didReachEndOfPage() {
        presenter.didReachEndOfPage()
    }
}

// MARK: - SearchBar Delegate
extension BreedsListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter.queryTextDidChange(searchText)
    }
}
