//
//  BreedsListCollectionViewDataSource.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import UIKit

protocol BreedsListCollectionViewDataSourceInput: UICollectionViewDataSource {

    func update(with items: [BreedProtocol])
}

final class BreedsListCollectionViewDataSource: NSObject, BreedsListCollectionViewDataSourceInput {

    private var items = [BreedProtocol]()

    func update(with items: [BreedProtocol]) {
        self.items = items
    }

    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        items.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withClass: BreedCollectionViewCell.self, for: indexPath)
        let item = items[indexPath.item]
        cell.configure(with: item)
        return cell
    }
}
