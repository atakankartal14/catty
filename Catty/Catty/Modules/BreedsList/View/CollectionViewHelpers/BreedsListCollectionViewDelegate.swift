//
//  BreedsListCollectionViewDelegate.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import UIKit

protocol BreedsListCollectionViewDelegateInput: UICollectionViewDelegate {

    var output: BreedsListCollectionViewDelegateOutput? { get set }
}

protocol BreedsListCollectionViewDelegateOutput: AnyObject {

    func didSelectItem(at index: Int)

    func didReachEndOfPage()
}

final class BreedsListCollectionViewDelegate: NSObject, BreedsListCollectionViewDelegateInput {
    
    private enum Constants {
        static let numberOfColumns: CGFloat = 2
        static let numberOfSpacingsInRow: CGFloat = numberOfColumns + 1
        static let paddings: CGFloat = 16
        static let ratio: CGFloat = 1.2
        static let edgeInset: CGFloat = 10
    }

    weak var output: BreedsListCollectionViewDelegateOutput?
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        output?.didSelectItem(at: indexPath.item)
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
            output?.didReachEndOfPage()
        }
    }
}

// MARK: - Flow Layout
extension BreedsListCollectionViewDelegate: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        let inset = Constants.edgeInset
        return UIEdgeInsets(top: inset, left: inset, bottom: inset, right: inset)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let totalSpacingInRow = Constants.numberOfSpacingsInRow * Constants.paddings
        let totalWidthOfRowsCells = collectionView.frame.width - totalSpacingInRow
        let cellWidth =  totalWidthOfRowsCells / Constants.numberOfColumns
        let cellHheight = cellWidth * Constants.ratio
        return CGSize(width: cellWidth, height: cellHheight)
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        Constants.paddings
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        Constants.paddings
    }
}
