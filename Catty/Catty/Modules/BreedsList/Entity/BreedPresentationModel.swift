//
//  BreedPresentationModel.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Foundation

protocol BreedProtocol {

    var id: String { get set }

    var name: String { get set }

    var imageUrl: URL? { get set }

    var temperament: String { get set }

    var energyLevel: String { get set }
}

struct BreedPresentationModel: BreedProtocol {

    var id: String
    var name: String
    var imageUrl: URL?
    var temperament: String
    var energyLevel: String

    init(from response: BreedsResponse) {
        self.id = response.id.orEmpty
        self.name = response.name.orEmpty
        if response.image == nil {
            let urlStr = APIConstants.breedImageUrlStr.replacingOccurrences(of: APIConstants.imageReferencePattern,
                                                                            with: response.referenceImageId.orEmpty)
            self.imageUrl = URL(string: urlStr)
        } else {
            self.imageUrl = response.image?.url.asURL
        }
        self.temperament = response.temperament.orEmpty
        self.energyLevel = response.energyLevel.orZero.asString + Constants.slash + Constants.energyLevelLimit
        
    }

    init() {
        self.id = ""
        self.name = ""
        self.imageUrl = nil
        self.temperament = ""
        self.energyLevel = ""
    }
}

private enum Constants {
    static let energyLevelLimit = "5"
    static let slash = " / "
}
