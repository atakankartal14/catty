//
//  Contracts.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Foundation

// MARK: - View
protocol BreedsListViewInput: AnyObject {

    func setTitle(_ text: String)

    func configureCollectionView()

    func reload(with items: [BreedProtocol])

    func showLoading()

    func hideLoading()
}

protocol BreedsListViewOutput: AnyObject {

    func viewDidLoad()
    
    func didReachEndOfPage()

    func didSelectItem(at index: Int)

    func queryTextDidChange(_ query: String)
}

// MARK: - Interactor
protocol BreedsListInteractorInput: AnyObject {

    func fetch()

    func fetchNextPage()

    func canFetchNext() -> Bool

    func search(_ query: String)
}

protocol BreedsListInteractorOutput: AnyObject {

    func interactor(_ interactor: BreedsListInteractorInput, didReceiveItems items: [BreedProtocol])

    func interactor(_ interactor: BreedsListInteractorInput, didReceiveQueryResults items: [BreedProtocol])
    
    func interactor(_ interactor: BreedsListInteractorInput, didFailWith error: APIError)
}

// MARK: - Router
protocol BreedsListRouterInput {

    func showDetail(with item: BreedProtocol)

    func showAlert(title: String, message: String)
}
