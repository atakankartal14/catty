//
//  Constants.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import UIKit

enum C {
    
    enum Strings {
        
        enum Title {
            static let breedsList = "Catty"
        }

        enum Errors {
            static let error = "Error!"
            static let statusCode = "Unexpected Status Code"
            static let decoding = "Decoding Error"
            static let failedDecoding = "Failed to decode object"
            
            static let noCell = "Couldn't find cell for: "
        }
        
        enum Text {
            static let ok = "OK"
            static let cancel = "Cancel"
            
            static let name = "Name"
            static let temperament = "Temperament"
            static let energyLevel = "Energy Level"
        }
    }

    enum Images {
        static let cat = UIImage(named: "cat")!
    }
}
