//
//  APIConstants.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Foundation

enum APIConstants {

    #if DEBUG
    typealias E = TestEnvironment
    #else
    typealias E = ProdEnvironment
    #endif

    static var headers: [String : String] {
        [
            "Content-Type": "application/json; charset=utf-8",
            "Accept": "application/vnd.github.v3+json",
            "x-api-key": E.apiKey
        ]
    }

    static var imageReferencePattern = "{image_reference_id}"

    static var breedImageUrlStr = "https://cdn2.thecatapi.com/images/\(imageReferencePattern).jpg"

}
