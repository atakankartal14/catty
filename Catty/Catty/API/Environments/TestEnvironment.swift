//
//  TestEnvironment.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Foundation

struct TestEnvironment: EnvironmentType {

    static var baseUrl: URL {
        URL(string: "https://api.thecatapi.com/v1")!
    }
    
    static var apiKey: String {
        "0141dd6c-b866-4124-b343-352b70310030"
    }
}
