//
//  API.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Foundation
import Moya

class API: APIType {

    #if DEBUG
    typealias E = TestEnvironment
    #else
    typealias E = ProdEnvironment
    #endif
    
    static let breedsProvider = MoyaProvider<BreedsTarget<E>>(plugins: TargetPlugins.plugins)
}
