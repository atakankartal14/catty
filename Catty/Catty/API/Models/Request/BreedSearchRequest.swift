//
//  BreedSearchRequest.swift
//  Catty
//
//  Created by Atakan Kartal on 8.04.2022.
//

import Foundation

struct BreedSearchRequest: Codable {

    let query: String
}

extension BreedSearchRequest {

    enum CodingKeys: String, CodingKey {
        case query = "q"
    }
}
