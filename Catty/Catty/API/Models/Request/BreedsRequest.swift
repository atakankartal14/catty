//
//  BreedsRequest.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Foundation

struct BreedsRequest: Codable {

    let page: Int?
    let limit: Int?
}
