//
//  APIError.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Moya

/// Base Represantable Error type for API
public struct APIError: Error {

    /// Error title value
    private(set) public var title: String

    /// Error message value
    private(set) public var message: String

    /// Error code
    private(set) public var code: Int?

    /// Status  code
    private(set) public var statusCode: Int?

    /// Custom Payload
    private(set) public var payload: Any?

    /// Response object
    private(set) public var response: Moya.Response?

    /// Init
    public init(title: String = "",
                message: String,
                code: Int? = nil,
                statusCode: Int? = nil,
                payload: Any? = nil,
                response: Moya.Response? = nil) {
        self.message = message
        self.title = title
        self.code = code
        self.statusCode = statusCode
        self.payload = payload
        self.response = response
    }
}
