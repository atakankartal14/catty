//
//  BreedsResponse.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Foundation

struct BreedsResponse: Codable {

    let id: String?
    let name: String?
    let image: Image?
    let referenceImageId: String?
    let temperament: String?
    let energyLevel: Int?
    
    struct Image: Codable {
        let url: String?
    }
}
