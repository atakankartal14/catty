//
//  Bundle+Ext.swift
//  Catty
//
//  Created by Atakan Kartal on 8.04.2022.
//

import Foundation

extension Bundle {

    static func loadJson(filename fileName: String) -> Data {
        if let url = main.url(forResource: fileName, withExtension: "json") {
            do {
                return try Data(contentsOf: url)
            } catch {
                debugPrint("error: \(error)")
                return .init()
            }
        } else {
            debugPrint("URL not found for Mock JSON")
            return .init()
        }
    }
}
