//
//  Dictionary+Ext.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Moya

extension Dictionary {

    /// Converts given dictionary to query string task
    public var urlEncodeQueryString: Task {
        guard let parameters = self.compactMapValues({ $0 }) as? [String: Any] else { return .requestPlain }
        return Task.requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
    }

}
