//
//  Encodable+Ext.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Foundation
import Moya

extension Encodable {

    /// Converts given encodable model to query string task
    var urlEncodedQueryString: Task {
        dictionary.urlEncodeQueryString
    }

    /// Converts given encodable model into dictionary
    var dictionary: [String: Any] {
        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .iso8601
        guard let data = try? encoder.encode(self) else {
            return [:]
        }
        let dict = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
        return dict ?? [:]
    }

}
