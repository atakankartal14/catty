//
//  CustomPlugin.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Moya
import UIKit

class CustomPlugin: PluginType {

    func willSend(_ request: RequestType, target: TargetType) {
        debugPrint("Request will send for: ", target.path)
        // TODO: Show Login
    }

    func didReceive(_ result: Result<Response, MoyaError>, target: TargetType) {
        debugPrint("Response did receive for: ", target.path)
        // TODO: Hide Login
    }
}
