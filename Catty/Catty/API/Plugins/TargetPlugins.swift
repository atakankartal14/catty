//
//  TargetPlugins.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Moya

struct TargetPlugins {

    static var plugins: [PluginType] = [
        CustomPlugin(),
        NetworkLoggerPlugin(configuration: NetworkLoggerPlugin.Configuration(logOptions: [.formatRequestAscURL,
                                                                                          .verbose]))
    ]
}
