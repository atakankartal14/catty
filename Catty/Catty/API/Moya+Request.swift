//
//  Moya+Request.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Foundation
import Moya

/// Custom request method
extension MoyaProvider {

    /// Generic Result Type
    public typealias APIResult<T: Codable> = (Result<T, APIError>) -> Void
    
    /// Constants typealias for easy access
    private typealias Const = C.Strings.Errors

    /// Custom API request method
    /// - Parameters:
    ///   - target: Target Type that the request will be sent for
    ///   - responseType: Expected response type of the request
    ///   - completion: Completion handler that returns APIResult 'MLBaseResponse<T>' or 'MLAPIError'
    /// - Returns: Request that is sent (in order to cancel at any point)
    @discardableResult
    public func request<T: Codable>(_ target: Target,
                                    responseType: T.Type,
                                    completion: @escaping APIResult<T>) -> Cancellable {
        sendRequest(target, responseType: responseType, completion: completion)
    }

    /// Custom Decoder
    private var jsonDecoder: JSONDecoder {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        return jsonDecoder
    }

    /// Sends request for given target
    private func sendRequest<T: Codable>(_ target: Target,
                                         responseType: T.Type,
                                         completion: @escaping APIResult<T>) -> Cancellable {
        request(target) { (result) in
            switch result {
            case .failure(let error):
                let error = APIError(title: Const.error, message: error.localizedDescription, code: error.response?.statusCode)
                completion(.failure(error))
            case .success(let response):
                guard self.validate(response, completion: completion) else { return }
                do {
                    let model = try response.map(T.self,
                                                 using: self.jsonDecoder,
                                                 failsOnEmptyData: false)
                    completion(.success(model))
                } catch MoyaError.statusCode(let response) {
                    let error = APIError(title: Const.error, message: Const.statusCode, code: response.statusCode, response: response)
                    completion(.failure(error))
                } catch let MoyaError.objectMapping(error, response) {
                    debugPrint("\(Const.decoding): ", error)
                    let error = APIError(title: Const.decoding, message: Const.failedDecoding, response: response)
                    completion(.failure(error))
                } catch {
                    let error = APIError(title: Const.error, message: error.localizedDescription, response: response)
                    completion(.failure(error))
                }
            }
        }
    }

    /// Validates the response for appropriate status code range
    private func validate<T: Codable>(_ response: Response, completion: @escaping APIResult<T>) -> Bool {
        do {
            _ = try response.filterSuccessfulStatusCodes()
            return true
        } catch {
            let error = APIError(title: Const.error,
                                 message: error.localizedDescription,
                                 code: nil,
                                 payload: nil,
                                 response: response)
            completion(.failure(error))
            return false
        }
    }
}
