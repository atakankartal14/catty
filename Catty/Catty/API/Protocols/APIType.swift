//
//  APIType.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Foundation

protocol APIType {

    associatedtype E: EnvironmentType
}
