//
//  EnvironmentType.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Foundation

/// Common interface for API environments
protocol EnvironmentType {

    /// Base API URL
    static var baseUrl: URL { get }

    /// API Key
    static var apiKey: String { get }
}
