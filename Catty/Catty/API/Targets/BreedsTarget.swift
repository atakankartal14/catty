//
//  BreedsTarget.swift
//  Catty
//
//  Created by Atakan Kartal on 7.04.2022.
//

import Foundation
import Moya

enum BreedsTarget<E: EnvironmentType> {

    case breeds(_ req: BreedsRequest)
    case search(_ req: BreedSearchRequest)
}

// MARK: - Target Type
extension BreedsTarget: TargetType {

    var baseURL: URL {
        E.baseUrl.appendingPathComponent("breeds")
    }
    
    var path: String {
        switch self {
        case .breeds:
            return ""
        case .search:
            return "search"
        }
    }
    
    var method: Moya.Method {
        .get
    }
    
    var task: Task {
        switch self {
        case .breeds(let req):
            return req.urlEncodedQueryString
        case .search(let req):
            return req.urlEncodedQueryString
        }
    }
    
    var headers: [String : String]? {
        APIConstants.headers
    }

    var sampleData: Data {
        switch self {
        case .breeds:
            return Bundle.loadJson(filename: "BreedsListMock")
        case .search:
            return Bundle.loadJson(filename: "BreedSearchMock")
        }
    }
}
