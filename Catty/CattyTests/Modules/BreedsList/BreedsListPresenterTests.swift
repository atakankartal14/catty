//
//  BreedsListPresenterTests.swift
//  CattyTests
//
//  Created by Atakan Kartal on 8.04.2022.
//

import XCTest
@testable import Catty

class BreedsListPresenterTests: XCTestCase {

    var sut: BreedsListPresenter!
    var interactor: BreedsListInteractorMock!
    var view: BreedsListViewMock!
    var router: BreedsListRouterMock!

    override func setUp() {
        super.setUp()
        interactor = .init()
        view = .init()
        router = .init()
        sut = .init(view: view, interactor: interactor, router: router)
        interactor.presenter = sut
    }

    override func tearDown() {
        sut = nil
        interactor = nil
        view = nil
        router = nil
        super.tearDown()
    }

    func test_viewDidLoad() {
        // Given
        interactor.shouldStop = true
        
        // When
        sut.viewDidLoad()
        
        // Then
        XCTAssertNotNil(view.title)
        XCTAssertTrue(view.collectionViewConfigured)
        XCTAssertTrue(view.isLoading)
        XCTAssertTrue(interactor.didStartFetch)
    }

    func test_didReachEndOfPage_startFetchNextPage() {
        // Given
        interactor.shouldFetchNext = true
        
        // When
        sut.didReachEndOfPage()
        
        // Then
        XCTAssertTrue(interactor.didStartFetch)
    }

    func test_didSelectItem_showsDetailPage() {
        // Given
        let generatedItems = BreedsListGenerator.generateItems()
        sut.interactor(interactor, didReceiveItems: generatedItems)
        
        // When
        sut.didSelectItem(at: 0)
        
        // Then
        XCTAssertTrue(router.didShowDetail)
        XCTAssertEqual(generatedItems[0].id, router.item?.id)
    }

    func test_didReceiveItems_updatesView() {
        // Given
        let generatedItems = BreedsListGenerator.generateItems()
        
        // When
        sut.interactor(interactor, didReceiveItems: generatedItems)
        
        // Then
        XCTAssertFalse(view.isLoading)
        XCTAssertTrue(view.reloaded)
        XCTAssertFalse(view.items.isEmpty)
    }

    func test_didReceiveQueryResult_updatesView() {
        // Given
        let generatedItems = BreedsListGenerator.generateItems()
        
        // When
        sut.interactor(interactor, didReceiveQueryResults: generatedItems)
        
        // Then
        XCTAssertFalse(view.isLoading)
        XCTAssertTrue(view.reloaded)
        XCTAssertFalse(view.items.isEmpty)
    }

    func test_didFailWith_hidesLoading_showsAlert() {
        // Given
        let error = APIError(title: "Title", message: "Error")
        
        // When
        sut.interactor(interactor, didFailWith: error)
        
        // Then
        XCTAssertFalse(view.isLoading)
        XCTAssertTrue(router.alertShowed)
        XCTAssertEqual(router.alertTitle, error.title)
        XCTAssertEqual(router.alertMessage, error.message)
    }

    func test_queryTextDidChange_notStartsSearchImmediately() {
        // When
        sut.queryTextDidChange("query")
        
        // Then
        XCTAssertFalse(view.isLoading)
        XCTAssertFalse(interactor.didStartSearch)
    }

    func test_queryTextDidChange_startsSearchAfterTimeGap() {
        // Given
        let expectation = XCTestExpectation(description: "Wait a second between queries")
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            expectation.fulfill()
        }
        
        // When
        sut.queryTextDidChange("query")

        // Then
        wait(for: [expectation], timeout: 2)

        XCTAssertTrue(interactor.didStartSearch)
    }
}
