//
//  BreedsListInteractorTests.swift
//  CattyTests
//
//  Created by Atakan Kartal on 8.04.2022.
//

import XCTest
import Moya
@testable import Catty

class BreedsListInteractorTests: XCTestCase {

    var sut: BreedsListInteractor!
    var presenter: BreedsListPresenterMock!

    override func tearDown() {
        sut = nil
        presenter = nil
        super.tearDown()
    }

    func test_fetch_failsWhenNetworkErrorOccured() {
        // Given
        sut = makeSut(with: MockAPI.breedsFail)
        
        // When
        sut.fetch()
        
        // Then
        XCTAssertTrue(presenter.didFail)
        XCTAssertNotNil(presenter.error)
    }

    func test_fetch_retrieveItems() {
        // Given
        sut = makeSut(with: MockAPI.breedsSuccess)
        
        // When
        sut.fetch()
        
        // Then
        XCTAssertFalse(presenter.didFail)
        XCTAssertNil(presenter.error)
        XCTAssertTrue(presenter.didReceiveItems)
        XCTAssertFalse(presenter.items.isEmpty)
    }

    func test_fetchNextPage_failsWhenNetworkErrorOccured() {
        // Given
        sut = makeSut(with: MockAPI.breedsFail)
        
        // When
        sut.fetchNextPage()
        
        // Then
        XCTAssertTrue(presenter.didFail)
        XCTAssertNotNil(presenter.error)
    }

    func test_fetchNextPage_retrieveItems() {
        // Given
        sut = makeSut(with: MockAPI.breedsSuccess)
        sut.fetch()
        
        // When
        sut.fetchNextPage()

        // Then
        XCTAssertFalse(presenter.didFail)
        XCTAssertNil(presenter.error)
        XCTAssertTrue(presenter.didReceiveItems)
        XCTAssertFalse(presenter.items.isEmpty)
    }

    func test_search_failsWhenNetworkErrorOccured() {
        // Given
        sut = makeSut(with: MockAPI.breedsFail)
        
        // When
        sut.search("query")
        
        // Then
        XCTAssertTrue(presenter.didFail)
        XCTAssertNotNil(presenter.error)
    }

    func test_search_retrieveQueryResults() {
        // Given
        sut = makeSut(with: MockAPI.breedsSuccess)
        
        // When
        sut.search("query")
        
        // Then
        XCTAssertFalse(presenter.didFail)
        XCTAssertNil(presenter.error)
        XCTAssertTrue(presenter.didReceiveQueryResults)
        XCTAssertFalse(presenter.items.isEmpty)
    }

    func test_search_resetsAndFetchsBreeds_whenQueryEmpty() {
        // Given
        let query = ""
        sut = makeSut(with: MockAPI.breedsSuccess)
        
        // When
        sut.search(query)
        
        // Then
        XCTAssertFalse(presenter.didFail)
        XCTAssertNil(presenter.error)
        XCTAssertTrue(presenter.didReceiveItems)
        XCTAssertFalse(presenter.items.isEmpty)
    }

    func test_canFetchNext_falseWhenFetching() {
        // Given
        sut = makeSut(with: MockAPI.breedsNeverResponds)
        
        // When
        sut.fetch()
        
        // Then
        XCTAssertFalse(sut.canFetchNext())
    }

    func test_canFetchNext_falseWhenSearching() {
        // Given
        sut = makeSut(with: MockAPI.breedsSuccess)
        
        // When
        sut.search("query")
        
        // Then
        XCTAssertFalse(sut.canFetchNext())
    }

    func test_canFetchNext_trueWhenFinishFetching() {
        // Given
        sut = makeSut(with: MockAPI.breedsSuccess)
        
        // When
        sut.fetch()
        
        // Then
        XCTAssertTrue(sut.canFetchNext())
    }

    func test_fetch_getsImageUrlFromImageResponseModel() {
        // Given
        sut = makeSut(with: MockAPI.breedsSuccess)
        
        // When
        sut.fetch()
        
        // Then
        XCTAssertNotNil(presenter.items[0].imageUrl)
    }

    func test_search_getsImageUrlFromImageReferenceId() {
        // Given
        sut = makeSut(with: MockAPI.breedsSuccess)
        
        // When
        sut.search("query")
        
        // Then
        XCTAssertNotNil(presenter.items[0].imageUrl)
    }

    /// Creates interactor with given provider
    private func makeSut(with provider: MoyaProvider<BreedsTarget<MockAPI.E>>) -> BreedsListInteractor {
        sut = BreedsListInteractor(provider: provider)
        presenter = BreedsListPresenterMock()
        sut.presenter = presenter
        return sut
    }
}
