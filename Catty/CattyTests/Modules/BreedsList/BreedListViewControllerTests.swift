//
//  BreedListViewControllerTests.swift
//  CattyTests
//
//  Created by Atakan Kartal on 9.04.2022.
//

import XCTest
@testable import ProgressHUD
@testable import Catty

final class BreedListViewControllerTests: XCTestCase {

    var sut: BreedsListViewController!
    var collectionViewDataSource: BreedListDataSourceMock!
    var collectionViewDelegate: BreedsListCollectionViewDelegateMock!
    var navigationController: UINavigationController!
    var presenter: BreedsListPresenterMock!

    override func setUp() {
        super.setUp()
        collectionViewDelegate = BreedsListCollectionViewDelegateMock()
        collectionViewDataSource = BreedListDataSourceMock()
        presenter = BreedsListPresenterMock()
        sut = BreedsListViewController(collectionViewDelegate: collectionViewDelegate,
                                       collectionViewDataSource: collectionViewDataSource)
        navigationController = UINavigationController(rootViewController: sut)
        sut.presenter = presenter
        presenter.view = sut
        sut.loadViewIfNeeded()
    }

    override  func tearDown() {
        collectionViewDelegate = nil
        collectionViewDataSource = nil
        presenter = nil
        sut = nil
        navigationController = nil
        super.tearDown()
    }

    func test_viewDidLoad_callsPresenter() {
        XCTAssertTrue(presenter.viewLoaded)
    }

    func test_setTitle() {
        // Given
        let expectedTitle = "Catty"
        
        // When
        sut.setTitle(expectedTitle)
        
        // Then
        XCTAssertEqual(sut.title, expectedTitle)
    }

    func test_configureCollectionView() {
        // Given
        XCTAssertNil(collectionViewDelegate.output)
        
        // When
        sut.configureCollectionView()
        
        // Then
        XCTAssertNotNil(collectionViewDelegate.output)
    }

    func test_reloadWithItems() {
        // Given
        XCTAssertTrue(collectionViewDataSource.items.isEmpty)
        let generatedItems = BreedsListGenerator.generateItems()
        
        // When
        sut.reload(with: generatedItems)
        
        // Then
        XCTAssertEqual(collectionViewDataSource.items.count, generatedItems.count)
    }

    func test_didSelectItem() {
        // Given
        XCTAssertFalse(presenter.itemSelected)
        XCTAssertNil(presenter.selectedItemIndex)
        let itemIndexToSelect = 5
        
        // When
        sut.didSelectItem(at: itemIndexToSelect)
        
        // Then
        XCTAssertTrue(presenter.itemSelected)
        XCTAssertEqual(presenter.selectedItemIndex, itemIndexToSelect)
    }

    func test_didReachEndOfPage() {
        // Given
        XCTAssertFalse(presenter.reachedEndOfPage)
        
        // When
        sut.didReachEndOfPage()
        
        // Then
        XCTAssertTrue(presenter.reachedEndOfPage)
    }

    func test_searchBarTextDidChange() {
        // Given
        let searchBar = UISearchBar()
        searchBar.delegate = sut
        XCTAssertFalse(presenter.queryTextChanged)
        XCTAssertEqual(presenter.query, "")
        let expectedQuery = "query"
        
        // When
        sut.searchBar(searchBar, textDidChange: expectedQuery)
        
        // Then
        XCTAssertTrue(presenter.queryTextChanged)
        XCTAssertEqual(presenter.query, expectedQuery)
    }
}
