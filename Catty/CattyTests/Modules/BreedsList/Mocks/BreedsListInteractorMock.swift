//
//  BreedsListInteractorMock.swift
//  CattyTests
//
//  Created by Atakan Kartal on 8.04.2022.
//

@testable import Catty

class BreedsListInteractorMock: BreedsListInteractorInput {
    
    weak var presenter: BreedsListInteractorOutput!

    var shouldStop = false
    var didStartFetch = false
    var shouldFetchNext = true
    var shouldFail = false
    var didStartSearch = false

    func fetch() {
        didStartFetch = true
        guard !shouldStop else {
            return
        }
        if shouldFail {
            presenter.interactor(self, didFailWith: .init(message: "Error"))
        } else {
            presenter.interactor(self, didReceiveItems: BreedsListGenerator.generateItems())
        }
    }

    func fetchNextPage() {
        fetch()
    }

    func canFetchNext() -> Bool {
        shouldFetchNext
    }

    func search(_ query: String) {
        didStartSearch = true
        guard !shouldStop else {
            return
        }
        if shouldFail {
            presenter.interactor(self, didFailWith: .init(message: "Error"))
        } else {
            presenter.interactor(self, didReceiveItems: BreedsListGenerator.generateItems())
        }
    }
}
