//
//  BreedListDataSourceMock.swift
//  CattyTests
//
//  Created by Atakan Kartal on 9.04.2022.
//

import UIKit
@testable import Catty

final class BreedListDataSourceMock: NSObject, BreedsListCollectionViewDataSourceInput {
    
    var updated = false
    var items = [BreedProtocol]()

    func update(with items: [BreedProtocol]) {
        self.updated = true
        self.items = items
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        .init()
    }
}
