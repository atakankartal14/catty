//
//  BreedsListGenerator.swift
//  CattyTests
//
//  Created by Atakan Kartal on 8.04.2022.
//

import Foundation
@testable import Catty

enum BreedsListGenerator {

    static func generateItems() -> [BreedProtocol] {
        var model = BreedPresentationModel()
        model.id = "1"
        model.name = "Name1"
        return [model]
    }
}
