//
//  BreedsListRouterMock.swift
//  CattyTests
//
//  Created by Atakan Kartal on 8.04.2022.
//

@testable import Catty

class BreedsListRouterMock: BreedsListRouterInput {

    var didShowDetail = false
    var item: BreedProtocol?
    var alertShowed = false
    var alertTitle = ""
    var alertMessage = ""
    
    func showDetail(with item: BreedProtocol) {
        didShowDetail = true
        self.item = item
    }
    
    func showAlert(title: String, message: String) {
        alertShowed = true
        alertTitle = title
        alertMessage = message
    }
}
