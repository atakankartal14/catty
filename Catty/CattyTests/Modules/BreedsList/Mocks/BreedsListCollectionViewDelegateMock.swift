//
//  BreedsListCollectionViewDelegateMock.swift
//  CattyTests
//
//  Created by Atakan Kartal on 9.04.2022.
//

import UIKit
@testable import Catty

final class BreedsListCollectionViewDelegateMock: NSObject, BreedsListCollectionViewDelegateInput {

    var output: BreedsListCollectionViewDelegateOutput?
}
