//
//  BreedsListViewMock.swift
//  CattyTests
//
//  Created by Atakan Kartal on 8.04.2022.
//

@testable import Catty

class BreedsListViewMock: BreedsListViewInput {
    
    var title: String?
    var collectionViewConfigured = false
    var items = [BreedProtocol]()
    var reloaded = false
    var isLoading = false

    func setTitle(_ text: String) {
        self.title = text
    }
    
    func configureCollectionView() {
        collectionViewConfigured = true
    }
    
    func reload(with items: [BreedProtocol]) {
        self.items = items
        reloaded = true
    }
    
    func showLoading() {
        isLoading = true
    }
    
    func hideLoading() {
        isLoading = false
    }
}
