//
//  BreedsListPresenterMock.swift
//  CattyTests
//
//  Created by Atakan Kartal on 8.04.2022.
//

@testable import Catty

class BreedsListPresenterMock {

    weak var view: BreedsListViewInput!
    
    // MARK: - View properties
    var viewLoaded = false
    var reachedEndOfPage = false
    var itemSelected = false
    var selectedItemIndex: Int?
    var queryTextChanged = false
    var query = ""
    
    // MARK: - Interactor properties
    var didReceiveItems = false
    var didReceiveQueryResults = false
    var items = [BreedProtocol]()
    var didFail = false
    var error: APIError?
}

// MARK: - View Output
extension BreedsListPresenterMock: BreedsListViewOutput {
    func viewDidLoad() {
        viewLoaded = true
    }
    
    func didReachEndOfPage() {
        reachedEndOfPage = true
    }
    
    func didSelectItem(at index: Int) {
        itemSelected = true
        selectedItemIndex = index
    }
    
    func queryTextDidChange(_ query: String) {
        queryTextChanged = true
        self.query = query
    }
}

// MARK: - Interactor Output
extension BreedsListPresenterMock: BreedsListInteractorOutput {

    func interactor(_ interactor: BreedsListInteractorInput, didReceiveItems items: [BreedProtocol]) {
        didReceiveItems = true
        self.items = items
    }

    func interactor(_ interactor: BreedsListInteractorInput, didReceiveQueryResults items: [BreedProtocol]) {
        didReceiveQueryResults = true
        self.items = items
    }

    func interactor(_ interactor: BreedsListInteractorInput, didFailWith error: APIError) {
        didFail = true
        self.error = error
    }
}
