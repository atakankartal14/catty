//
//  BreedDetailInteractorTests.swift
//  CattyTests
//
//  Created by Atakan Kartal on 8.04.2022.
//

import XCTest
@testable import Catty

class BreedDetailInteractorTests: XCTestCase {

    var sut: BreedDetailInteractor!
    var presenter: BreedDetailPresenterMock!
    
    override func setUp() {
        super.setUp()
        presenter = BreedDetailPresenterMock()
        sut = .init(breed: BreedsListGenerator.generateItems()[0])
        sut.presenter = presenter
    }

    override func tearDown() {
        sut = nil
        presenter = nil
        super.tearDown()
    }

    func test_fetch_returnsModel() {
        // Given
        let expectedItemCount = 3

        // When
        sut.fetch()
        
        // Then
        XCTAssertTrue(presenter.didReceiveModel)
        XCTAssertNotNil(presenter.model)
        XCTAssertEqual(presenter.model?.items.count, expectedItemCount)
    }
}
