//
//  BreedDetailGenerator.swift
//  CattyTests
//
//  Created by Atakan Kartal on 8.04.2022.
//

import Foundation
@testable import Catty

enum BreedDetailGenerator {

    static func generateModel() -> BreedDetailPresentationProtocol {
        let url = URL(string: "www.google.com")
        let item = BreedDetailItemPresentationModel(title: "Title", detail: "Detail")
        return BreedDetailPresentationModel(imageUrl: url, items: [item])
    }
}
