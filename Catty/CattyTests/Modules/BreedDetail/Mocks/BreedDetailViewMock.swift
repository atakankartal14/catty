//
//  BreedDetailViewMock.swift
//  CattyTests
//
//  Created by Atakan Kartal on 8.04.2022.
//

import Foundation
@testable import Catty

final class BreedDetailViewMock: BreedDetailViewInput {
    
    var presenter: BreedDetailViewOutput!
    
    var didConfigureTableView = false
    var didReload = false
    var model: BreedDetailPresentationProtocol?
    
    func configureTableView() {
        didConfigureTableView = true
    }
    
    func reload(with model: BreedDetailPresentationProtocol) {
        didReload = true
        self.model = model
    }
}
