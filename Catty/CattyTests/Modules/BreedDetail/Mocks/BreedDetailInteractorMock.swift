//
//  BreedDetailInteractorMock.swift
//  CattyTests
//
//  Created by Atakan Kartal on 8.04.2022.
//

import Foundation
@testable import Catty

final class BreedDetailInteractorMock: BreedDetailInteractorInput {

    weak var presenter: BreedDetailInteractorOutput!
    
    var isFetched = true
    
    func fetch() {
        isFetched = true
        presenter.interactor(self, didReceiveModel: BreedDetailGenerator.generateModel())
    }
}
