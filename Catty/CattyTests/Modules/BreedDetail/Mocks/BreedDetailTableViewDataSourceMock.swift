//
//  BreedDetailTableViewDataSourceMock.swift
//  CattyTests
//
//  Created by Atakan Kartal on 9.04.2022.
//

import UIKit
@testable import Catty

final class BreedDetailTableViewDataSourceMock: NSObject, BreedDetailTableViewDataSourceInput {

    var items = [BreedDetailItemProtocol]()
    var isUpdated = false

    func update(with items: [BreedDetailItemProtocol]) {
        isUpdated = true
        self.items = items
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        .init()
    }
}
