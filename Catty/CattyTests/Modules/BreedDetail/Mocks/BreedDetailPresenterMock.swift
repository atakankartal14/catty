//
//  BreedDetailPresenterMock.swift
//  CattyTests
//
//  Created by Atakan Kartal on 8.04.2022.
//

import Foundation
@testable import Catty

final class BreedDetailPresenterMock {
    
    weak var view: BreedDetailViewInput!
    
    // MARK: - View properties
    var viewLoaded = false
    
    // MARK: - Interactor properties
    var didReceiveModel = false
    var model: BreedDetailPresentationProtocol?
}

// MARK: - View Output
extension BreedDetailPresenterMock: BreedDetailViewOutput {

    func viewDidLoad() {
        viewLoaded = true
    }
}

// MARK: - Interactor Output
extension BreedDetailPresenterMock: BreedDetailInteractorOutput {

    func interactor(_ interactor: BreedDetailInteractorInput, didReceiveModel model: BreedDetailPresentationProtocol) {
        didReceiveModel = true
        self.model = model
    }
}
