//
//  BreedDetailTableViewDelegateMock.swift
//  CattyTests
//
//  Created by Atakan Kartal on 9.04.2022.
//

import Foundation
@testable import Catty

final class BreedDetailTableViewDelegateMock: NSObject, BreedDetailTableViewDelegateInput {

    var isUpdated = false
    var url: URL?

    func update(with url: URL?) {
        isUpdated = true
        self.url = url
    }

}
