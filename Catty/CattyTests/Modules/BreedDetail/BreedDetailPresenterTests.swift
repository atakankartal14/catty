//
//  BreedDetailPresenterTests.swift
//  CattyTests
//
//  Created by Atakan Kartal on 8.04.2022.
//

import XCTest
@testable import Catty

class BreedDetailPresenterTests: XCTestCase {

    var sut: BreedDetailPresenter!
    var interactor: BreedDetailInteractorMock!
    var view: BreedDetailViewMock!

    override func setUp() {
        super.setUp()
        interactor = .init()
        view = .init()
        sut = .init(view: view, interactor: interactor)
        interactor.presenter = sut
    }

    override func tearDown() {
        sut = nil
        interactor = nil
        view = nil
        super.tearDown()
    }

    func test_viewDidload_configuresView_startsFetch() {
        // When
        sut.viewDidLoad()
        
        // Then
        XCTAssertTrue(view.didConfigureTableView)
        XCTAssertTrue(interactor.isFetched)
    }

    func test_didReceiveModel_reloadsView() {
        // Given
        let expectedItemCount = 1
        
        // When
        sut.viewDidLoad()
        
        // Then
        XCTAssertTrue(view.didReload)
        XCTAssertNotNil(view.model)
        XCTAssertEqual(view.model?.items.count, expectedItemCount)
    }
}
