//
//  BreedDetailViewControllerTests.swift
//  CattyTests
//
//  Created by Atakan Kartal on 9.04.2022.
//

import XCTest
@testable import Catty

class BreedDetailViewControllerTests: XCTestCase {

    var sut: BreedDetailViewController!
    var tableViewDataSource: BreedDetailTableViewDataSourceMock!
    var tableViewDelegate: BreedDetailTableViewDelegateMock!
    var presenter: BreedDetailPresenterMock!

    override func setUp() {
        super.setUp()
        tableViewDataSource = BreedDetailTableViewDataSourceMock()
        tableViewDelegate = BreedDetailTableViewDelegateMock()
        presenter = BreedDetailPresenterMock()
        sut = BreedDetailViewController(tableViewDelegate: tableViewDelegate,
                                        tableViewDataSource: tableViewDataSource)
        sut.presenter = presenter
        presenter.view = sut
        sut.loadViewIfNeeded()
    }

    override  func tearDown() {
        tableViewDelegate = nil
        tableViewDataSource = nil
        presenter = nil
        sut = nil
        super.tearDown()
    }

    func test_viewDidLoad() {
        XCTAssertTrue(presenter.viewLoaded)
    }

    func test_reloadWith() {
        // Given
        XCTAssertFalse(tableViewDataSource.isUpdated)
        XCTAssertTrue(tableViewDataSource.items.isEmpty)
        XCTAssertFalse(tableViewDelegate.isUpdated)
        XCTAssertNil(tableViewDelegate.url)
        let generatedModel = BreedDetailGenerator.generateModel()
        
        // When
        sut.reload(with: generatedModel)
        
        // Then
        XCTAssertTrue(tableViewDataSource.isUpdated)
        XCTAssertEqual(tableViewDataSource.items.count, generatedModel.items.count)
        XCTAssertTrue(tableViewDelegate.isUpdated)
        XCTAssertEqual(tableViewDelegate.url, generatedModel.imageUrl)
    }
}
