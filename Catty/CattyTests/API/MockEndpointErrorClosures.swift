//
//  MockEndpointErrorClosures.swift
//  CattyTests
//
//  Created by Atakan Kartal on 8.04.2022.
//

@testable import Catty
import Foundation
import Moya

enum MockEndpointErrorClosures {

    static var fetchErrorClosure = { (target: BreedsTarget<TestEnvironment>) -> Endpoint in
        return Endpoint(url: URL(target: target).absoluteString,
                        sampleResponseClosure: { .networkResponse(400, .init()) },
                        method: target.method,
                        task: target.task,
                        httpHeaderFields: target.headers)
    }
}
