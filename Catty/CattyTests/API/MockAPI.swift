//
//  MockAPI.swift
//  CattyTests
//
//  Created by Atakan Kartal on 8.04.2022.
//

import Moya
@testable import Catty

class MockAPI: APIType {
    
    typealias E = TestEnvironment

    static let breedsSuccess = MoyaProvider<BreedsTarget<E>>.init(stubClosure: MoyaProvider.immediatelyStub)

    static let breedsNeverResponds = MoyaProvider<BreedsTarget<E>>.init(stubClosure: MoyaProvider.neverStub)
    
    static let breedsFail = MoyaProvider<BreedsTarget<E>>.init(endpointClosure: MockEndpointErrorClosures.fetchErrorClosure, stubClosure: MoyaProvider.immediatelyStub)
}
