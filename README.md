# Catty
---

![list](https://www.linkpicture.com/q/list_6.png) ![search](https://www.linkpicture.com/q/search_3.png) ![search](https://www.linkpicture.com/q/detail_3.png) 


## Overview
---
Catty is an encyclopedia application to show some adorable cats


## Features
---
- List the breeds of cats with their cute pictures
- Search for specific type of breed
- See breed's detailed explanation


## Tech Stack
---
- Xcode: Version 13.1
- Language: Swift 5.5.1
- Minimum iOS Version: 14.0
- Dependency Manager: SPM


## Dependencies
---
| Dependency | Reason |
| ------ | ------ |
| [Moya](https://github.com/Moya/Moya) | Network abstraction layers |
| [ProgressHud](https://github.com/relatedcode/ProgressHUD) | Loading animations |


## Architecture
---
![VIPER Module Representation](https://i.stack.imgur.com/bpM7t.png)

_VIPER_ is selected as the project's architectural pattern. Project may seem small for now however, at some point in the future new features can be added or the development team may grow. _VIPER_ helps us at scaling the project. It separates responsibilities of components and increases testability also helps to keep consistency of code styling throughout the project

## Unit Tests
---
##### 76.5 % Unit test coverage
-  Breeds List (ViewController, Presenter, Interactor)
-  Breed Detail (ViewController, Presenter, Interactor)


## Limitations
---
Provied [API](https://thecatapi.com) has different endpoints for listing and searching breeds so, complexity of networking logic increases. If a single endpoint can be constructed for both actions, that helps networking logic's getting simpler


## Installation
---
Clone the repository to your local
```bash
git clone git@bitbucket.org:atakankartal14/catty.git
```
Run Catty.xcodeproj


## Contributing
---
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.


## License
---
MIT
